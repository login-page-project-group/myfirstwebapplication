package com.abhishek.springboot.myfirstwebapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFirsWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyFirsWebApplication.class, args);
    }

}
