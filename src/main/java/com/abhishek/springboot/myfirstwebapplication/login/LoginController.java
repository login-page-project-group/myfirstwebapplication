package com.abhishek.springboot.myfirstwebapplication.login;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Abhishek Sheelwant
 */
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String goToLoginPage(){
        return "login";
    }

}
